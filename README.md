# README #

### What is this repository for? ###

* Exercise test for ING interview
* Version 0.0.1-SNAPSHOT didn't make a release version

### How do I get set up? ###

* Introduction and build: : It is a maven application so building the jar with dependencies is done standard by running "mvn clean install" in the root directory with the parent pom.xml.
It uses spring boot for running and set-up, spring controller for the endpoint, spring core for the dependency injection, spring data and JPA for saving the entities: the user and the associated savings account.
I have created a very simple web client using jquery and ajax that calls the endpoint.

* Run : I have created a spring boot aplication in order to start it, run ActuatorServiceApplication.java.
Or even better directly run the packaged jar java -jar target\savings-service-0.0.1-SNAPSHOT.jar.

* Urls
http://localhost:8080/h2 - Console access for the in memory database where you can see the result of the operations in the 2 tables: SAVINGS_ACCOUNT and USER. Connection details from application.properties.
http://localhost:8080/actuator/health - Spring boot actuator health status

http://localhost:8080/index.html - The very simple web client 
http://localhost:8080/openSavingsAccount - The endpoint - POST

* Input/output
Accepted input sample {"email":"a@a.com","firstName":"Cristian","lastName":"Stamate"}
Retunred output sample {
    "id": 4,
    "iban": "57833230-be46-4dc7-8672-b1bc7c3932c3",
    "accountBalance": 0,
    "status": "OPEN"
}

* Database configuration
All the database configuration is application.properties

* More implementation details
Different validation errors for the 3 input fields.
Validation on format of the e-mail field.

The unicity of the user is verified through the e-mail, so you will have one account balance per e-mail. Saving twice with the same e-mail will display an error message.
Interceptor for checking if the endpoint should be available with name OpenedInterceptor.java.(As requested the account can only be opened within working days and only between 9 AM and 6 PM ).

*Unit tests
SavingsServiceImplTest.java unit tests for the service class using mocks.


### Who do I talk to? ###

* stamatefcristian@gmail.com