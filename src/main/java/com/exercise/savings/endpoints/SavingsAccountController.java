package com.exercise.savings.endpoints;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.exercise.savings.exception.UserAlreadyHasAccountException;
import com.exercise.savings.model.SavingsAccount;
import com.exercise.savings.model.User;
import com.exercise.savings.service.SavingsService;

@Controller
public class SavingsAccountController {
	
	private static final Logger log = LoggerFactory.getLogger(SavingsAccountController.class);
	
	@Autowired
	SavingsService savingsService;
	
	/**
	 * @param user
	 * @return
	 * Main endpoint responsable for the treatment of user savings accounts
	 */
	@PostMapping("/openSavingsAccount")
	@ResponseBody
	public SavingsAccount openSavingsAccount(@Valid @RequestBody User user) {
		
		log.info("Valid request on openSavingsAccount endpoint started");
		
		boolean savingAccountExists = savingsService.accountExistsFor(user.getEmail());
		
		if(savingAccountExists) {
			throw new UserAlreadyHasAccountException("User with this e-mail already has account");
		}else {
			log.info("Succesfull request");
			
			return savingsService.saveAccount(user);
		}
		
		
	}

}
