package com.exercise.savings.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.PRECONDITION_FAILED)
public class ServiceNotOpenedException extends RuntimeException {

	private static final long serialVersionUID = 6769348965138063137L;
	
	public ServiceNotOpenedException(String message) {
		super(message);
	}

}
