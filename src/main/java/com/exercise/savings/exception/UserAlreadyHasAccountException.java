package com.exercise.savings.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
public class UserAlreadyHasAccountException extends RuntimeException {

	private static final long serialVersionUID = -5916161927847815739L;
	
	public UserAlreadyHasAccountException(String message) {
		super(message);
	}

}
