package com.exercise.savings.interceptor;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.exercise.savings.exception.ServiceNotOpenedException;

public class OpenedInterceptor extends HandlerInterceptorAdapter {
	
	@Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if(isOpened()) {
			return true;
		}else {
			throw new ServiceNotOpenedException("Service is not opened");
		}
	}
	
	private boolean isOpened() {
		LocalDate localDate = LocalDate.now();
		DayOfWeek dayOfWeek = localDate.getDayOfWeek();
		
		//calculate if it is a working day
		if(dayOfWeek.equals(DayOfWeek.SATURDAY) || dayOfWeek.equals(DayOfWeek.SUNDAY)) {
			return false;
		}
		LocalTime localTime = LocalTime.now();
		int hour  = localTime.getHour();
		
		//calculate if it is after working hours
		if(hour<9 || hour>18) {
			return false;
		}
		return true;
	}


}
