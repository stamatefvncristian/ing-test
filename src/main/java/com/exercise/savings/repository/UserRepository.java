package com.exercise.savings.repository;

import org.springframework.data.repository.CrudRepository;

import com.exercise.savings.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
	
	User findByEmail(String email);

}
