package com.exercise.savings.service;

import com.exercise.savings.model.SavingsAccount;
import com.exercise.savings.model.User;

public interface SavingsService {

	/**
	 * @param user
	 * @return
	 * method for saving the user with the needed acount
	 */
	SavingsAccount saveAccount(User user);

	/**
	 * @param userEmail
	 * @return
	 * verification that for a given user email
	 */
	boolean accountExistsFor(String userEmail);

}
