package com.exercise.savings.service;

import java.util.UUID;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exercise.savings.model.AccountStatus;
import com.exercise.savings.model.SavingsAccount;
import com.exercise.savings.model.User;
import com.exercise.savings.repository.UserRepository;

@Service
@Transactional
public class SavingsServiceImpl implements SavingsService {
	
	private static final Logger log = LoggerFactory.getLogger(SavingsServiceImpl.class);
	
	@Autowired
	UserRepository userRepository;
	
	@Override
	public boolean accountExistsFor(String userEmail) {
		
		log.info("checking if account exists");
		
		User user = userRepository.findByEmail(userEmail);
		if(user != null && user.getSavingsAccount() != null) {
			log.info("acount already exists");
			return true;
		}
		log.info("account doesn't exists for this user email");
		return false;
	}
	
	@Override
	public SavingsAccount saveAccount(User user) {
		SavingsAccount savingsAccount = new SavingsAccount();
		savingsAccount.setStatus(AccountStatus.OPEN);
		savingsAccount.setIban(generateIban());
		user.setSavingsAccount(savingsAccount);
		
		User returnedUser = userRepository.save(user);
		
		log.info("account saved succesfully");
		return returnedUser.getSavingsAccount();
	}
	
	/**
	 * fast generation method, doesn't look like an IBAN of course
	 */
	private String generateIban() {
		return UUID.randomUUID().toString();
	}

}
