package com.exercise.savings.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.exercise.savings.model.AccountStatus;
import com.exercise.savings.model.SavingsAccount;
import com.exercise.savings.model.User;
import com.exercise.savings.repository.UserRepository;

@RunWith(MockitoJUnitRunner.class) 
public class SavingsServiceImplTest {
	
	@Mock
	UserRepository userRepository;
	
	@InjectMocks
	SavingsServiceImpl savingsService = new SavingsServiceImpl();
	
	@Test
	public void shoudSaveAccount() {
		//given
		User user = new User();
		user.setEmail("a@a.com");
		user.setFirstName("firstName");
		user.setLastName("lastName");
		when(userRepository.save(any(User.class))).thenReturn(user);
		//when
		savingsService.saveAccount(user);
		//then
		ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);
		verify(userRepository).save(captor.capture());
		User filledUser = captor.getValue();
		assertEquals(filledUser.getSavingsAccount().getStatus(), AccountStatus.OPEN);
		assertNotNull(filledUser.getSavingsAccount().getIban());
		
	}
	
	@Test
	public void shouldCheckForExistingAccount() {
		//given
		String userEmail = "someEmail@some.com";
		User user = new User();
		SavingsAccount savingsAccount = new SavingsAccount();
		user.setSavingsAccount(savingsAccount);
		when(userRepository.findByEmail(userEmail)).thenReturn(user);
		
		//when
		boolean exists = savingsService.accountExistsFor(userEmail);
		
		//then
		assertTrue(exists);
	}
	
	@Test
	public void shouldCheckForNonExistingAccount() {
		//given
		String userEmail = "someEmail@some.com";
		User user = new User();
		when(userRepository.findByEmail(userEmail)).thenReturn(user);
		
		//when
		boolean exists = savingsService.accountExistsFor(userEmail);
		
		//then
		assertFalse(exists);
	}

}
